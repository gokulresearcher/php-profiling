SHELL=/bin/sh
.DEFAULT_GOAL := help
.SILENT:
.PHONY: vendor

## Colors
COLOR_RESET   = \033[0m
COLOR_INFO    = \033[32m
COLOR_COMMENT = \033[33m

## Help
help:
	printf "${COLOR_COMMENT}Usage:${COLOR_RESET}\n"
	printf " make [target]\n\n"
	printf "${COLOR_COMMENT}Available targets:${COLOR_RESET}\n"
	awk '/^[a-zA-Z\-\_0-9\.@]+:/ { \
		helpMessage = match(lastLine, /^## (.*)/); \
		if (helpMessage) { \
			helpCommand = substr($$1, 0, index($$1, ":")); \
			helpMessage = substr(lastLine, RSTART + 3, RLENGTH); \
			printf " ${COLOR_INFO}%-16s${COLOR_RESET} %s\n", helpCommand, helpMessage; \
		} \
	} \
	{ lastLine = $$0 }' $(MAKEFILE_LIST)

define exec
	docker-compose exec app $(1) 2> /dev/null || $(1)
endef

define run
	docker-compose run --rm app $(1) 2> /dev/null || $(1)
endef

ifneq (,$(findstring sonar,$(MAKECMDGOALS)))
phpunit_options := $(phpunit_options) --coverage-clover build/reports/coverage.xml --log-junit build/reports/tests.xml
endif

##################
# Useful targets #
##################

## Prepare app
app: build-swoole start

## Start app
start:
	docker-compose up -d

## Stop app
stop:
	docker-compose down

## Build swoole image
build-swoole:
	docker build --build-arg DEV_MODE=true -t php-performance_phpswoole -f ./swoole80/config/docker/Dockerfile .

## Benchmark all containers
benchmark: benchmark-swoole benchmark-nginx-php-fpm-80 benchmark-nginx-php-fpm-74

## Benchmark nginx with php-fpm:7.4
benchmark-nginx-php-fpm-74:
	ab -n 500 -c 25 http://localhost:8074/

## Benchmark nginx with php-fpm:8.0
benchmark-nginx-php-fpm-80:
	ab -n 500 -c 25 http://localhost:8080/

## Benchmark swoole
benchmark-swoole:
	ab -n 500 -c 25 http://localhost:8086/